<?php

namespace App\Controllers;

use App\Models\User;

class HomeController extends Controller
{

    public function index($request, $response)
    {
        //$this->flash->addMessage('error','Test flash Message');
        return $this->view->render($response, 'home.twig');
    }
}
